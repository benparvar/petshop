# BootcampAlliance

---
# Architecture

## Clear Architecture

* Infrastructure -> Itens relacionados a parte mais externa da aplica��o, ou seja, itens relacionados a aplica��o em si.

* Interfaceadapter -> Itens relacionados ao que o usu�rio acessa/� acessado. Exemplos: Controllers, Gateways e Presenters.

* Businessrule -> S�o os casos de uso, ou seja, as regras de neg�cio da aplica��o.

* Domain -> S�o as entidades de dom�nio, ou seja, relativos as regras de neg�cio do dom�nio.

![Alt text](images/clean_architecture.png?raw=true "Clean Architecture Cone")

---
# Libraries

## Java

#### OpenJdk 11

* https://openjdk.java.net/

## Gradle

#### Gradle 6.8.3

* https://gradle.org/

## Spring

#### Spring-Boot 2.4.5

* https://spring.io/projects/spring-boot

#### Spring-Security 5.46

* https://spring.io/projects/spring-security

## Mapstruct

#### Mapstruct 1.4.2.Final

* https://mapstruct.org/

## Lombok

#### Lombok 1.18.20

* https://projectlombok.org/

## JUnit

#### JUnit 5.7.0

* https://junit.org/

## Swagger

#### Swagger 2.9.2

## Mockito

#### Mockito 3.0.0

* https://site.mockito.org/

## H2 Database Engine

#### H2 1.4.200

* http://www.h2database.com/html/main.html

#### Console

* URL: http://localhost:8080/h2

* Driver Class: org.h2.Driver

* JDBC URL: jdbc:h2:mem:mock_db

* User Name: sa

* Password:

## API Documentation 

* Run the application and see http://localhost:8080/swagger-ui.html

---
# Build and Run

## How to delete the build folder

* Execute in the terminal: $ ./gradlew deleteBuildFolder


## How to build

* Execute in the terminal: $ ./gradlew clean build


## How to run the application

* Execute in the terminal: $ ./gradlew bootRun

* URL: http://localhost:8080/actuator

## How to run integration tests

*  Execute in the terminal:

* (default localhost): petshop/integration$ ../gradlew -PintegrationTests test

* (with server and port): petshop/integration$ ../gradlew -PintegrationTests -Dhostname=http://server.il -Dport=9876 test


## How to call a service

* Authorization Type: Basic Auth

    * Try https://www.blitter.se/utils/basic-authentication-header-generator/

        * Username: TBL_USERS.login

        * Pasword: TBL_USERS.password

    * Example:

        * Execute in the terminal: $ curl --location --request GET 'http://localhost:8080/api/v1/users?name={name}' \
          --header 'Authorization: Basic YWxhbnM6ZjFiZjIyNmMwZDA0MDg2ZGY5MjBjNTAyOGRlOWRkMjY=' \
          --header 'Cookie: JSESSIONID=AC376154B02CBA31CA1E5AEAC2E33AEB'
        
