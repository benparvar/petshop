package com.benparvar.petshop.businessrule.pet;

import com.benparvar.petshop.domain.Pet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetPetByNameUseCaseTest {

    private GetPetByNameUseCase getPetByNameUseCase;
    private final GetPetByName getPetByName = mock(GetPetByName.class);

    @BeforeEach
    public void init() {
        getPetByNameUseCase = new GetPetByNameUseCase(getPetByName);
    }

    @Test
    public void executeGetPetByNameWithoutError() {
        when(getPetByName.getByName(Mockito.anyString())).thenReturn(Optional.of(Pet.builder().name(UUID.randomUUID().toString()).build()));

        Optional<Pet> pet = getPetByNameUseCase.execute(UUID.randomUUID().toString());

        assertNotNull(pet);
        assertNotNull(pet.get().getName());
    }
}