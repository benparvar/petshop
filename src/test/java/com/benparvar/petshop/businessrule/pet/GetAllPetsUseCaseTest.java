package com.benparvar.petshop.businessrule.pet;

import com.benparvar.petshop.domain.Pet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetAllPetsUseCaseTest {

    private static final int ZERO = 0;
    private static final int ONE = 1;

    private GetAllPetsUseCase getAllPetsUseCase;
    private final GetAllPets getAllPets = mock(GetAllPets.class);

    @BeforeEach
    public void init() {
        getAllPetsUseCase = new GetAllPetsUseCase(getAllPets);
    }

    @Test
    public void executeWithANonEmptyPetList() {
        when(getAllPets.getAll()).thenReturn(Optional.of(Arrays.asList(
                Pet.builder().name("Luke").build(),
                Pet.builder().name("Estrela").build(),
                Pet.builder().name("Belinha").build(),
                Pet.builder().name("Tonico").build())));

        Optional<List<Pet>> petList = getAllPetsUseCase.execute();

        assertNotNull(petList);
        assertFalse(petList.isEmpty());
    }

    @Test
    public void executeWithPetListWithOnePet() {
        when(getAllPets.getAll()).thenReturn(Optional.of(Collections.singletonList(Pet.builder().name("Luke").build())));

        Optional<List<Pet>> petList = getAllPetsUseCase.execute();

        assertNotNull(petList);
        assertEquals(ONE, petList.get().stream().count());
    }

    @Test
    public void executeWithAEmptyPetList() {
        when(getAllPets.getAll()).thenReturn(Optional.of(Collections.emptyList()));

        Optional<List<Pet>> petList = getAllPetsUseCase.execute();

        assertNotNull(petList);
        assertEquals(ZERO, petList.get().stream().count());
    }

    @Test
    public void executeWithANullPetList() {
        when(getAllPets.getAll()).thenReturn(null);

        Optional<List<Pet>> petList = getAllPetsUseCase.execute();

        assertNull(petList);
    }
}