package com.benparvar.petshop.businessrule.user;

import com.benparvar.petshop.domain.User;

import java.util.Optional;

public interface FindUserByLoginAndPassword {
    Optional<User> findByLoginAndPassword(String login, String password);
}
