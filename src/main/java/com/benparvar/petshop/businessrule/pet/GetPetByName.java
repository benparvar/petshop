package com.benparvar.petshop.businessrule.pet;

import com.benparvar.petshop.domain.Pet;

import java.util.Optional;

public interface GetPetByName {
    Optional<Pet> getByName(String name);
}
