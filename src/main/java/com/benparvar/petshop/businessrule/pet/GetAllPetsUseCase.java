package com.benparvar.petshop.businessrule.pet;

import com.benparvar.petshop.domain.Pet;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class GetAllPetsUseCase {

    private final GetAllPets getAllPets;

    public Optional<List<Pet>> execute() {
        return getAllPets.getAll();
    }
}
