package com.benparvar.petshop.businessrule.pet;

import com.benparvar.petshop.domain.Pet;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class GetPetByNameUseCase {

    private final GetPetByName getPetByName;

    public Optional<Pet> execute(String name) {
        return getPetByName.getByName(name);
    }
}
