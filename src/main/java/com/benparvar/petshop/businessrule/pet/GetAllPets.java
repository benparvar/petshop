package com.benparvar.petshop.businessrule.pet;

import com.benparvar.petshop.domain.Pet;

import java.util.List;
import java.util.Optional;

public interface GetAllPets {
    Optional<List<Pet>> getAll();
}
