package com.benparvar.petshop.interfaceadapter.controller;

import com.benparvar.petshop.businessrule.pet.GetAllPetsUseCase;
import com.benparvar.petshop.domain.Pet;
import com.benparvar.petshop.domain.exception.PetEmptyListException;
import com.benparvar.petshop.interfaceadapter.controller.model.GetPetResponse;
import com.benparvar.petshop.interfaceadapter.controller.model.GetPetResponseMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class PetController {
    private final GetAllPetsUseCase getAllPetsUseCase;

    public List<GetPetResponse> getAll() {

        return GetPetResponseMapper.INSTANCE.entityToResponse(Optional.of(getAllPetsUseCase.execute()).get()
                .orElseThrow(PetEmptyListException::new));
    }
}
