package com.benparvar.petshop.interfaceadapter.controller.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetPetResponse {
    private String breed;
    private String name;
}
