package com.benparvar.petshop.interfaceadapter.controller.model;

import com.benparvar.petshop.domain.Pet;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface GetPetResponseMapper {
    GetPetResponseMapper INSTANCE = Mappers.getMapper(GetPetResponseMapper.class);

    GetPetResponse entityToResponse(Pet entity);

    List<GetPetResponse> entityToResponse(List<Pet> entity);
}
