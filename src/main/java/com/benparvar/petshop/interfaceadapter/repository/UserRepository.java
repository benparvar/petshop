package com.benparvar.petshop.interfaceadapter.repository;


import com.benparvar.petshop.interfaceadapter.repository.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserModel, Long> {

    UserModel findByLoginAndPassword(String login, String password);
}
