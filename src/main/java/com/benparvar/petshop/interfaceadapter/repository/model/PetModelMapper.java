package com.benparvar.petshop.interfaceadapter.repository.model;

import com.benparvar.petshop.domain.Pet;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface PetModelMapper {
    PetModelMapper INSTANCE = Mappers.getMapper(PetModelMapper.class);

    Pet modelToEntity(PetModel model);

    List<Pet> modelToEntity(List<PetModel> model);
}
