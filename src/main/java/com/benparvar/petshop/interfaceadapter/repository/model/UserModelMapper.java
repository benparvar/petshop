package com.benparvar.petshop.interfaceadapter.repository.model;

import com.benparvar.petshop.domain.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserModelMapper {
    UserModelMapper INSTANCE = Mappers.getMapper(UserModelMapper.class);

    User modelToEntity(UserModel model);
}
