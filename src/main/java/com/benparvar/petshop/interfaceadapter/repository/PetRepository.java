package com.benparvar.petshop.interfaceadapter.repository;

import com.benparvar.petshop.interfaceadapter.repository.model.PetModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PetRepository extends JpaRepository<PetModel, Long> {

    Optional<PetModel> getByName(String name);
}
