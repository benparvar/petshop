package com.benparvar.petshop.interfaceadapter.gateway;


import com.benparvar.petshop.businessrule.user.FindUserByLoginAndPassword;
import com.benparvar.petshop.domain.User;
import com.benparvar.petshop.interfaceadapter.repository.UserRepository;
import com.benparvar.petshop.interfaceadapter.repository.model.UserModelMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class UserGateway implements FindUserByLoginAndPassword {
    private final UserRepository userRepository;

    @Override
    public Optional<User> findByLoginAndPassword(String login, String password) {
        return Optional.ofNullable(UserModelMapper.INSTANCE.modelToEntity(userRepository.findByLoginAndPassword(login, password)));
    }
}
