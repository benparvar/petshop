package com.benparvar.petshop.interfaceadapter.gateway;

import com.benparvar.petshop.businessrule.pet.GetAllPets;
import com.benparvar.petshop.businessrule.pet.GetPetByName;
import com.benparvar.petshop.domain.Pet;
import com.benparvar.petshop.interfaceadapter.repository.PetRepository;
import com.benparvar.petshop.interfaceadapter.repository.model.PetModelMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class PetGateway implements GetAllPets, GetPetByName {
    private final PetRepository petRepository;

    @Override
    public Optional<List<Pet>> getAll() {

        return Optional.ofNullable(PetModelMapper.INSTANCE.modelToEntity(petRepository.findAll()));
    }

    @Override
    public Optional<Pet> getByName(String name) {
        return Optional.empty();
    }
}
