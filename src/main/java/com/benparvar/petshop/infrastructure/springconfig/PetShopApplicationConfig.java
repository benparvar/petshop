package com.benparvar.petshop.infrastructure.springconfig;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan("com.benparvar.petshop")
@EnableJpaRepositories("com.benparvar.petshop.interfaceadapter.repository")
@EntityScan("com.benparvar.petshop.interfaceadapter.repository.model")
public class PetShopApplicationConfig {
}
