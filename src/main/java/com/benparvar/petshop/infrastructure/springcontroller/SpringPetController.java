package com.benparvar.petshop.infrastructure.springcontroller;

import com.benparvar.petshop.interfaceadapter.controller.PetController;
import com.benparvar.petshop.interfaceadapter.controller.model.GetPetResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@Slf4j
@Component
@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/pets", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "Pet")
public class SpringPetController {
    private final PetController petController;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Get all pets", authorizations = {
            @Authorization(
                    value = "basic",
                    scopes = {
                            @AuthorizationScope(
                                    scope = "get:/api/v1/pets",
                                    description = "get a list of all pets")
                    }
            )})
    public List<GetPetResponse> getAll() {
        log.info("get all");

        return petController.getAll();
    }

}
