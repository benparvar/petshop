package com.benparvar.petshop.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Pet {
    private String breed;
    private String name;
}
